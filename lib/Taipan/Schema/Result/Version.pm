use utf8;
package Taipan::Schema::Result::Version;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Taipan::Schema::Result::Version

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::EncodedColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "EncodedColumn");

=head1 TABLE: C<version>

=cut

__PACKAGE__->table("version");

=head1 ACCESSORS

=head2 db_version

  data_type: 'text'
  is_nullable: 0

=head2 db_version_date

  data_type: 'date'
  is_nullable: 1

=head2 db_comment

  data_type: 'text'
  is_nullable: 1

=head2 created_at

  data_type: 'timestamp with time zone'
  default_value: timezone('utc'::text, now())
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "db_version",
  { data_type => "text", is_nullable => 0 },
  "db_version_date",
  { data_type => "date", is_nullable => 1 },
  "db_comment",
  { data_type => "text", is_nullable => 1 },
  "created_at",
  {
    data_type     => "timestamp with time zone",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</db_version>

=back

=cut

__PACKAGE__->set_primary_key("db_version");


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2018-02-21 18:46:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2Tc+5avDDROV7QhS0vKWXQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
